package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    ArrayList < FridgeItem > items = new ArrayList();
    private final int maxCap = 20;



    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return maxCap;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if(items.size() < maxCap){
            items.add(item);
            return true; 
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if(!items.contains(item)){
            throw new NoSuchElementException();
        }
        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        items.removeAll(items);
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList < FridgeItem > exitems = new ArrayList();
        for(int i = 0; i < items.size(); i++){
            if (items.get(i).hasExpired()){
                exitems.add(items.get(i));
            }
        }
        items.removeAll(exitems);
        return exitems;
    }
    
}
